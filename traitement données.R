rm(list=ls())
getwd()

#import####
library(readxl)
data_emissions <- read_excel("Data_Extract_From_World_Development_Indicators.xlsx")
class(data_emissions$`Country Name`)
data_emissions$`Country Name` <- as.factor(data_emissions$`Country Name`) 
area_emissions <- levels(data_emissions$`Country Name`)#268 levels (régions + pays)

data_whw_ratio <- read_excel("Ratio_weekly_hours_worked_by_pop.xlsx", skip=5)
class(data_whw_ratio$`Reference area`)
data_whw_ratio$`Reference area` <- as.factor(data_whw_ratio$`Reference area`) 
area_whw_ratio <- levels(data_whw_ratio$`Reference area`)#283 levels (régions + pays)

data_whw <- read_excel("Total_weekly_hors_worked.xlsx", skip=5)
class(data_whw$`Reference area`)
data_whw$`Reference area` <- as.factor(data_whw$`Reference area`) 
area_whw <- levels(data_whw$`Reference area`)#283 levels (régions + pays)

#levels en différence####
setdiff(area_whw_ratio, area_whw)#les mêmes levels dans les 2 tables (juste pour être sûr)
setdiff(area_emissions, area_whw)#vérification des différences de levels
setdiff(area_whw, area_emissions)
#Pays au nom différent (emis /// whw):
list_pays_diff = matrix(c("Bahamas, The", "Bahamas",
                          "Cabo Verde", "Cape Verde",
                          "Congo, Dem. Rep.", "Congo, Democratic Republic of the",
                          "Congo, Rep.", "Congo",
                          "Cote d'Ivoire", "Côte d'Ivoire",
                          "Czech Republic", "Czechia",
                          "Egypt, Arab Rep.", "Egypt",
                          "Gambia, The", "Gambia",
                          "Hong Kong SAR, China", "Hong Kong, China",
                          "Iran, Islamic Rep.", "Iran, Islamic Republic of",
                          "Korea, Dem. People's Rep.", "Korea, Democratic People's Republic of",
                          "Korea, Rep.", "Korea, Republic of",
                          "Kyrgyz Republic", "Kyrgyzstan",
                          "Lao PDR", "Lao People's Democratic Republic",
                          "Macao SAR, China", "Macau, China",
                          "Moldova", "Moldova, Republic of",
                          "Slovak Republic", "Slovakia",
                          "St. Lucia", "Saint Lucia",
                          "St. Vincent and the Grenadines", "Saint Vincent and the Grenadines",
                          "Tanzania", "Tanzania, United Republic of",
                          "Venezuela, RB", "Venezuela, Bolivarian Republic of",
                          "Vietnam", "Viet Nam",
                          "West Bank and Gaza", "Occupied Palestinian Territory",
                          "Yemen, Rep.", "Yemen"), ncol = 2, byrow = TRUE)
for (i in 1:nrow(list_pays_diff)) {
  data_emissions[data_emissions == list_pays_diff[i,1]] <- list_pays_diff[i,2]
} # ne pas convertir en facteurs

#seulement dans emis :
#Andorra
#Antigua and Barbuda
#Dominica
#Grenada
#Kiribati
#Liechtenstein
#Marshall Islands
#Micronesia, Fed. Sts.
#Monaco
#Nauru
#Palau
#San Marino
#Seychelles
#St. Kitts and Nevis
#Tuvalu

#seulement dans whw :
#Taiwan, China

#levels en commun
intersect(area_whw, area_emissions)
#à part le total (World), que des pays et territoires ont déjà le même label.
#Pas d'autres régions en doublon, yay

#fusion####
data_whw <- subset(data_whw, select = -Source)
data_whw_ratio <- subset(data_whw_ratio, select = -Source)
names(data_whw)[names(data_whw) == "Value"] <- "Total weekly hours worked"
names(data_whw_ratio)[names(data_whw_ratio) == "Value"] <- "Ratio weekly hours worked by population"
data <- merge(data_whw, data_whw_ratio, by=c("Reference area", "Time"))
names(data_emissions)[names(data_emissions) == "Country Name"] <- "Reference area"
data <- merge(data_emissions, data, by=c("Reference area", "Time"))
data$`CO2 emissions (metric tons per capita) [EN.ATM.CO2E.PC]` <-
  as.numeric(data$`CO2 emissions (metric tons per capita) [EN.ATM.CO2E.PC]`)
data$`CO2 emissions (kt) [EN.ATM.CO2E.KT]` <-
  as.numeric(data$`CO2 emissions (kt) [EN.ATM.CO2E.KT]`)
data$`CO2 emissions (kg per 2010 US$ of GDP) [EN.ATM.CO2E.KD.GD]` <-
  as.numeric(data$`CO2 emissions (kg per 2010 US$ of GDP) [EN.ATM.CO2E.KD.GD]`)
data$`GDP per capita (constant 2010 US$) [NY.GDP.PCAP.KD]` <-
  as.numeric(data$`GDP per capita (constant 2010 US$) [NY.GDP.PCAP.KD]`)
data$`GDP (constant 2010 US$) [NY.GDP.MKTP.KD]` <-
  as.numeric(data$`GDP (constant 2010 US$) [NY.GDP.MKTP.KD]`)
data$`Labor force, total [SL.TLF.TOTL.IN]` <-
  as.numeric(data$`Labor force, total [SL.TLF.TOTL.IN]`)
data$`Labor force with intermediate education (% of total working-age population with intermediate education) [SL.TLF.INTM.ZS]` <-
  as.numeric(data$`Labor force with intermediate education (% of total working-age population with intermediate education) [SL.TLF.INTM.ZS]`)
data$`Labor force with basic education (% of total working-age population with basic education) [SL.TLF.BASC.ZS]` <-
  as.numeric(data$`Labor force with basic education (% of total working-age population with basic education) [SL.TLF.BASC.ZS]`)
data$`Labor force with advanced education (% of total working-age population with advanced education) [SL.TLF.ADVN.ZS]` <-
  as.numeric(data$`Labor force with advanced education (% of total working-age population with advanced education) [SL.TLF.ADVN.ZS]`)
data$`Labor force participation rate, total (% of total population ages 15-64) (modeled ILO estimate) [SL.TLF.ACTI.ZS]` <-
  as.numeric(data$`Labor force participation rate, total (% of total population ages 15-64) (modeled ILO estimate) [SL.TLF.ACTI.ZS]`)

data_world <- subset(data, `Reference area` == "World")
data <- subset(data, `Reference area` != "World")

colnames(data) <- c("Pays", "Année", "Code pays", "Code année", "Emissions CO2 (tonne par tete)", "Emissions CO2 (kT)", "Emissions CO2 (kg par PIB ($2010)", 
                    "PIB par tete ($2010 constant)", "PIB ($2010 constant)", "Population active totale", "Taux d'activité des diplomés intermédiaires (%)",
                    "Taux d'activité des diplomés basiques (%)", "Taux d'activité des diplomés du supérieur (%)", "Taux d'activité (%)", 
                    "Heures travaillées hebdomadaire totales", "Heures travaillées hebdomadaires par population")

save(data, file = "data.Rda")
